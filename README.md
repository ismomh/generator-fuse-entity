# Générateur de module crud pour le theme fuse2 par rapport à jhipster

## Installation

Pour l'installation vous placé le dosssier quelque part sur machine.
Ensuite si vous aviez installer yeoman:
 * Avec npm
```
npm link
```
 * Avec yarn
```
yarn link
```
NB: Link crée un raccourci du dossier au niveau globale. pour yeoman puisse le voir.

## Exemple d'utilisation
Supposont qu'on veut générer un crud pour une entité personne.

```bash
yo fuse-entity main/content/personne
```
Ensuite vous n'aurez qu'a suivre le questionnaire.

## PS

 * Dans le fichier "app.constants.ts" ajouter une consante "export const ITEMS_PER_PAGE = 20;". Sa répresente la valeur par défaut des pages.
 * La génération ajoutera un petit composant "my-paginator" dans le chemin "src/app/core/component/my-paginator". Alors il vous faudra le déclarer et l'exporté dans le module "shared.module".
 * Il y'aura un autre fichier appeler 'my-entity-utils.ts' qui contient une fonction. ce fichier sera sur le chemin "src/app/core/utils/my-entity-utils.ts".
 


## License

MIT © [Ismaël M. Harouna]()

