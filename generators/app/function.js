module.exports = {
    getJsType,
    getValidator
}

function getJsType(fieldType){
    if(fieldType == 'Text' || fieldType == 'TextLong'){
        return 'string';
    }else if(fieldType == 'Chiffre'){
        return 'number';
    }else if(fieldType == 'Boolean'){
        return 'boolean';
    }else if(fieldType == 'Date'){
        return 'Date';
    }else{
        'any';
    }
}

function getValidator(field) {
    let val = "";

    if(field.fieldValidation.includes('required')){
        val += 'Validators.required, ';
    }

    if(field.fieldValidation.includes('min')){
        if(getJsType(field.fieldType) == 'number'){
            val += 'Validators.min('+ field.min +'), ';
        }else{
            val += 'Validators.minLength('+ field.min +'), ';
        }
    }
    
    if(field.fieldValidation.includes("max")){
        if(getJsType(field.fieldType) == 'number'){
            val += 'Validators.max('+ field.max +'), ';
        }else{
            val += 'Validators.maxLength('+ field.max +'), ';
        }
    }
    
    return val;
}
