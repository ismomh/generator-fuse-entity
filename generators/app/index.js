"use strict";
const Generator = require("yeoman-generator");
const chalk = require("chalk");
const yosay = require("yosay");
const path = require("path");
const fs = require("fs");
const _ = require("lodash");
const util = require("./function");

module.exports = class extends Generator {

  constructor(args, opts) {
    super(args, opts);
    this.argument("path", {
      type: String,
      required: true,
      description: "Nom de l'entité"
    });
  }

  initializing() {
    this.options["name"] = path.posix.basename(this.options.path);
    this.fieldTab = [];
  }

  prompting() {
    this.log(
      yosay(
        "Salut les gas. Voici " +
          chalk.red("generator-fuse-entity") +
          " generator!"
      )
    );

    const prompts = [
      {
        type: "input",
        name: "routeAngular",
        message: "Route angular",
        default: this.options.name
      },
      {
        type: "input",
        name: "routeApi",
        message: "Route Api",
        default: () => {
          let suf = this.options.name.endsWith("s") ? "es" : "s";
          return `api/${this.options.name}${suf}`;
        }
      }
    ];

    return this.prompt(prompts).then(props => {
      this.entity = props;
      this.entity.path = "src/app/" + this.options.path;
      this.entity.name = this.options.name;
    });
  }

  askForField() {
    const done = this.async();
    const prompts = [
      {
        type: "confirm",
        message: "Voulez vous ajouter un champs?",
        name: "addField",
        default: true
      },
      {
        when: response => response.addField === true,
        type: "input",
        name: "fieldName",
        message: "Quel est le nom votre champs?",
        validate: input => {
          if (input === "") {
            return "Le champs ne peut pas etre vide";
          }
          return true;
        }
      },
      {
        when: response => response.addField === true,
        type: "list",
        name: "fieldType",
        message: "Quel est le type du champs?",
        choices: [
          { value: "Text", name: "Texte" },
          { value: "TextLong", name: "Texte long" },
          { value: "Chiffre", name: "Chiffre" },
          { value: "Boolean", name: "Boolean" },
          { value: "Date", name: "Date" },
          { value: "ChoixSelect", name: "Choix (select)" }
        ],
        default: 0
      },
      {
        when: response => response.addField === true,
        type: "checkbox",
        name: "fieldValidation",
        message: "Quelle validation voulez vous ajouter?",
        choices: [
          { name: "Required", value: "required" },
          { name: "Minimum", value: "min" },
          { name: "Maximum", value: "max" }
        ],
        default: 0
      },
      {
        when: response =>
          response.addField === true &&
          response.fieldValidation.includes("min"),
        type: "input",
        name: "min",
        message: "Quelle est la valeur minimale?",
        default: 0
      },
      {
        when: response =>
          response.addField === true &&
          response.fieldValidation.includes("max"),
        type: "input",
        name: "max",
        message: "Quelle est la valeur maximale?",
        default: 100
      }
    ];

    return this.prompt(prompts).then(props => {
      if (props.addField) {
        props.validators = util.getValidator(props);
        props.fieldNameCapitalize = _.upperFirst(props.fieldName);
        props.jsType = util.getJsType(props.fieldType);
        this.fieldTab.push(props);     
        this.askForField();
      }else{
        done();
      }
    });

  }

  writing() {

    let pathEntityUtils = "src/app/core/utils/my-entity-utils.ts";
    let pathMyPaginator = "src/app/core/components/my-paginator/my-paginator.component.";
    
    if(!this.fs.exists(this.destinationPath(pathEntityUtils))){
      this.fs.copy(
        this.templatePath("utils/my-entity-utils.ts"),
        this.destinationPath(pathEntityUtils)
      );
    }

    if(!this.fs.exists(this.destinationPath(pathMyPaginator+"ts"))){
      this.fs.copy(
        this.templatePath("my-paginator/my-paginator.component.ts"),
        this.destinationPath(pathMyPaginator+"ts")
      );
      this.fs.copy(
        this.templatePath("my-paginator/my-paginator.component.html"),
        this.destinationPath(pathMyPaginator+"html")
      );
      this.fs.copy(
        this.templatePath("my-paginator/my-paginator.component.scss"),
        this.destinationPath(pathMyPaginator+"scss")
      );
    }


    let folder = this.templatePath('entity');
    let self  = this;

    copyAll(folder);

    function copyAll(fd) {
      fs.readdirSync(fd).forEach(file => {
        if(fs.lstatSync(fd+"/"+file).isDirectory()){
          copyAll(fd+"/"+file);
        }else{
          let fdDest = path.basename(fd).replace('entity', self.entity.name);
          let fileDest = file.replace('entity', self.entity.name);

          let pathDest = self.entity.path + "/";
          let pathSrc = fd + "/" + file;

          if(fdDest == self.entity.name){
            pathDest += fileDest;
          }else{
            pathDest += fdDest + "/" + fileDest
          }

          self.fs.copyTpl(
            self.templatePath(pathSrc),
            self.destinationPath(pathDest),
            {
              name: self.entity.name.toLowerCase(),
              nameCapitalise: _.upperFirst(_.camelCase(self.entity.name)),
              routeApi: self.entity.routeApi,
              routeAngular: self.entity.routeAngular,
              fields: self.fieldTab,
              util: util
            }
          );
        }
      });
    }
    
  }

  end() {

    this.log("Ouf.. " + chalk.green("c'est terminer") + "!!!");
  }

};
