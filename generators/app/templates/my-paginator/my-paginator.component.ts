import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { fuseAnimations } from '../../animations';

@Component({
  selector: 'app-my-paginator',
  templateUrl: './my-paginator.component.html',
  styleUrls: ['./my-paginator.component.scss'],
  animations: fuseAnimations
})
export class MyPaginatorComponent implements OnInit {
  private _total:number;
 
  @Input() pageSize:number;
  @Input() page:number;
  @Output() selectPage = new EventEmitter<number>();

  @Input() 
  set total(total:number){
    this._total = total;
    this.nbrPage = Math.ceil(this._total / this.pageSize);
    this.numbers = Array(this.nbrPage).fill(0).map((x,i)=>i);
  }

  get total(){
    return this._total;
  }

  private numbers;
  nbrPage;

  constructor() {}

  ngOnInit() {
    this.nbrPage = Math.ceil(this._total / this.pageSize);    
    this.numbers = Array(this.nbrPage).fill(0).map((x,i)=>i);
  }

  pageSelect(p){
    
    this.page = p;
    this.selectPage.emit(this.page);
  }

  changePage(n){
    this.pageSelect(n);
  }

  next(){
    if(this.page < this.nbrPage-1)
    this.pageSelect(this.page+1);
  }

  prev(){
    if(this.page > 0)
    this.pageSelect(this.page-1);
  }

  last(){
    if(this.page < this.nbrPage-1)
    this.pageSelect(this.nbrPage-1);
  }

  first(){
    if(this.page > 0)
    this.pageSelect(0);
  }

}
