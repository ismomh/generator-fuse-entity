import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { <%= nameCapitalise %>Component } from './<%= name %>.component';
import { SharedModule } from '../../../core/modules/shared.module';
import { <%= nameCapitalise %>ListComponent } from './<%= name %>-list/<%= name %>-list.component';
import { <%= nameCapitalise %>Service } from './<%= name %>.service';
import { AuthGuard } from '../../../core/services/auth/auth.guard';
import { <%= nameCapitalise %>FormComponent } from './<%= name %>-form/<%= name %>-form.component';

const route:Route[] = [
  {
    path: '<%= routeAngular %>',
    component: <%= nameCapitalise %>Component,
    resolve: {
      <%= name %>: <%= nameCapitalise %>Service
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(route)
  ],
  providers: [
    <%= nameCapitalise %>Service
  ],
  declarations: [
    <%= nameCapitalise %>Component, 
    <%= nameCapitalise %>ListComponent, <%= nameCapitalise %>FormComponent
  ],
  entryComponents:[<%= nameCapitalise %>FormComponent]
})
export class <%= nameCapitalise %>Module { }
