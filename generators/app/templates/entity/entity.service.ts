import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { Observable } from "rxjs/Observable";  

import { SERVER_API_URL, ITEMS_PER_PAGE } from "../../../core/app.constants";
import { HttpClient, HttpParams } from "@angular/common/http";

@Injectable()
export class <%= nameCapitalise %>Service implements Resolve<any> {
  resourceUrl = SERVER_API_URL + '<%= routeApi %>';
  <%= name %>Tab: any[];
  page:number = 0;
  totalCount:number;
  on<%= nameCapitalise %>Changed: BehaviorSubject<any> = new BehaviorSubject({});
  onTotal<%= nameCapitalise %>Changed: BehaviorSubject<any> = new BehaviorSubject({});
  constructor(private http: HttpClient) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    return this.get<%= nameCapitalise %>();
  }

  get<%= nameCapitalise %>(page?, size?): Promise<any> {
    const data = this.getParams(page, size);

    return new Promise((resolve, reject) => {
      this.http
        .get(this.resourceUrl, {params: data, observe:"response"})
        .subscribe((response: any) => {
          this.<%= name %>Tab = response.body;
          const total = response.headers.get("X-Total-Count");
          this.totalCount = total;
          if(total)
          this.onTotal<%= nameCapitalise %>Changed.next({total:total, page: data.page});
          this.on<%= nameCapitalise %>Changed.next(this.<%= name %>Tab);
          resolve();
        });
    });
  }

  getParams(p?, s?): any{
    this.page = (p != null)? p : this.page;
    s = (s)? s :  ITEMS_PER_PAGE;

    const nbrPage = Math.ceil(this.totalCount/s);
    if(this.page >= nbrPage)
    this.page = nbrPage-1;

    return {page:this.page, size:s};
  }


  update<%= nameCapitalise %>(<%= name %>): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http
        .put(this.resourceUrl, <%= name %>)
        .subscribe(response => {
          this.get<%= nameCapitalise %>();
          resolve(response);
        });
    });
  }

  delete<%= nameCapitalise %>(<%= name %>): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http
        .delete(`${this.resourceUrl}/${<%= name %>.id}`, {responseType: 'text' })
        .subscribe(() => {
          this.totalCount--;
          this.get<%= nameCapitalise %>();
          resolve();
        });
    });
  }

}
