import { Component, OnInit, ViewChild } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { <%= nameCapitalise %>Service } from '../<%= name %>.service';
import { Observable } from 'rxjs/Observable';
import { fuseAnimations } from '../../../../core/animations';
import { MatDialogRef, MatDialog, MatPaginator } from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../core/components/confirm-dialog/confirm-dialog.component';
import { <%= nameCapitalise %>FormComponent } from '../<%= name %>-form/<%= name %>-form.component';
import { FormGroup } from '@angular/forms';
import { MyCustomSnackBar } from '../../../../core/services/shared/my-snackbar.service';
import { ITEMS_PER_PAGE } from '../../../../core/app.constants';

@Component({
  selector: "app-<%= name %>-list",
  templateUrl: "./<%= name %>-list.component.html",
  styleUrls: ["./<%= name %>-list.component.scss"],
  animations: fuseAnimations
})
export class <%= nameCapitalise %>ListComponent implements OnInit {
  displayedColumns = [<%_ fields.forEach(field => { _%>"<%= field.fieldName %>", <%_}); _%> "buttons"];
  dataSource: FilesDataSource | null;
  totalCount;
  pageSize = ITEMS_PER_PAGE;
  page = 0;
  dialogRef: any;
  confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

  constructor(
    private <%= name %>Service: <%= nameCapitalise %>Service,
    public dialog: MatDialog,
    private snackBar: MyCustomSnackBar
  ) {}

  ngOnInit() {
    this.dataSource = new FilesDataSource(this.<%= name %>Service);
    this.<%= name %>Service
    .onTotal<%= nameCapitalise %>Changed.subscribe(response => {
      this.totalCount = response.total;
      this.page = response.page;
    });
  }


  edit(<%= name %>) {
    this.dialogRef = this.dialog.open(<%= nameCapitalise %>FormComponent, {
      panelClass: '<%= name %>-form-dialog',
      data      : {
        <%= name %>: <%= name %>,
          action : 'edit'
      }
  });

  this.dialogRef.afterClosed()
      .subscribe(response => {
          if ( !response )
          {
              return;
          }
          const actionType: string = response[0];
          const formData: FormGroup = response[1];
          switch ( actionType )
          {
            case 'save':
              this.<%= name %>Service.update<%= nameCapitalise %>(formData.getRawValue());
              break;
            case 'delete':
              this.delete<%= nameCapitalise %>(<%= name %>);
              break;
          }
      });
  }

  delete<%= nameCapitalise %>(<%= name %>){
    this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
        disableClose: false
    });

    this.confirmDialogRef.componentInstance.confirmMessage = 'Vous êtes sur de vouloir faire la suppresion?';

    this.confirmDialogRef.afterClosed().subscribe(result => {
        if ( result )
        {
            this.<%= name %>Service.delete<%= nameCapitalise %>(<%= name %>).then((res) => {
              this.snackBar.msgSuppressionSuccee();
            });
        }
        this.confirmDialogRef = null;
    });
  }

  onPageSelect(page){
    this.page = page;
    this.<%= name %>Service.get<%= nameCapitalise %>(this.page, this.pageSize);
  }

}

export class FilesDataSource extends DataSource<any> {
  
  constructor(private <%= name %>Service: <%= nameCapitalise %>Service) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<any[]> {
    return this.<%= name %>Service.on<%= nameCapitalise %>Changed;
  }

  disconnect() {}
}
