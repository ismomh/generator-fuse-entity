
export class <%= nameCapitalise %> {
    constructor(
        public id?: number,
        <%_ fields.forEach(field => { _%>
        public <%= field.fieldName %>?: <%= util.getJsType(field.fieldType) %>,   
        <%_}); _%>
    ) {
    }
}
