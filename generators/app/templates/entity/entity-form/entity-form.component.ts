import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { <%= nameCapitalise %> } from '../<%= name %>.model';
import { myPopulateFormErrors } from '../../../../core/utils/my-entity-utils';

@Component({
  selector: 'app-<%= name %>-form',
  templateUrl: './<%= name %>-form.component.html',
  styleUrls: ['./<%= name %>-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class <%= nameCapitalise %>FormComponent implements OnInit {
  
  dialogTitle: string;

  <%= name %>Form: FormGroup;
  <%= name %>FormErrors: any;

  action: string;
  <%= name %>: <%= nameCapitalise %>;
  constructor(
    public dialogRef: MatDialogRef<<%= nameCapitalise %>FormComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private formBuilder: FormBuilder
  ) {
    this.action = data.action;
    
    if ( this.action === 'edit' )
    {
        this.dialogTitle = 'Modification';
        this.<%= name %> = data.<%= name %>;
    }
    else
    {
        this.dialogTitle = 'Nouveau';
        this.<%= name %> = new <%= nameCapitalise %>();
    }

    
  }

  create<%= nameCapitalise %>Form(): any {
    return this.formBuilder.group({
      id: [this.<%= name %>.id],
      <%_ fields.forEach(field => { _%>
      <%= field.fieldName %>: [this.<%= name %>.<%= field.fieldName %>,[<%= field.validators %>]],   
      <%_}); _%>
    });
  }

  ngOnInit() {

    this.<%= name %>Form = this.create<%= nameCapitalise %>Form();
    
    this.<%= name %>Form.valueChanges.subscribe(() => {
      this.<%= name %>FormErrors = myPopulateFormErrors(this.<%= name %>Form, ['id']);
    });
    
  }

  
}
