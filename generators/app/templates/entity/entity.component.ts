import { Component, OnInit } from '@angular/core';
import { fuseAnimations } from '../../../core/animations';
import { MatDialog } from '@angular/material';
import { <%= nameCapitalise %>FormComponent } from './<%= name %>-form/<%= name %>-form.component';
import { FormGroup } from '@angular/forms';
import { <%= nameCapitalise %>Service } from './<%= name %>.service';
import { MyCustomSnackBar, SUCCESS } from '../../../core/services/shared/my-snackbar.service';

@Component({
  selector: 'app-<%= name %>',
  templateUrl: './<%= name %>.component.html',
  styleUrls: ['./<%= name %>.component.scss'],
  animations: fuseAnimations
})
export class <%= nameCapitalise %>Component implements OnInit {
  dialogRef: any;
  constructor(
    public dialog: MatDialog,
    private <%= name %>Service: <%= nameCapitalise %>Service,
    private snackBar: MyCustomSnackBar
  ) { }

  ngOnInit() {
  }

  new<%= nameCapitalise %>(){
    this.dialogRef = this.dialog.open(<%= nameCapitalise %>FormComponent, {
      panelClass: '<%= name %>-form-dialog',
      data      : {
          action: 'new'
      }
  });

  this.dialogRef.afterClosed()
      .subscribe((response: FormGroup) => {
          if ( !response )
          {
              return;
          }

          this.<%= name %>Service.update<%= nameCapitalise %>(response.getRawValue())
          .then((res) => {
            this.snackBar.msgEnregistrementSuccee();
          });

      });
  }

}
