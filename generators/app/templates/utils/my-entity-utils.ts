import { FormGroup } from "@angular/forms";
import { HttpParams } from "@angular/common/http";


export function myPopulateFormErrors(form: FormGroup, fieldIGnore?: String[]): any{
    let formErrors = myGetFormErros(form, fieldIGnore);

    for ( const field in formErrors )
    {
        if ( !formErrors.hasOwnProperty(field) )
        {
            continue;
        }

        // Clear previous errors
        formErrors[field] = {};

        // Get the control
        const control = form.get(field);

        if ( control && control.dirty && !control.valid )
        {
            formErrors[field] = control.errors;
        }
    }
    return formErrors;
}

function myGetFormErros(form: FormGroup, fieldIGnore?: String[]): any{
    let formErros = {};
    for(const field in form.controls){
        if(fieldIGnore && fieldIGnore.length > 0){
            if(fieldIGnore.indexOf(field))
                formErros[field] = {};
        }else
            formErros[field] = {};
        
    }
    return formErros;
}
